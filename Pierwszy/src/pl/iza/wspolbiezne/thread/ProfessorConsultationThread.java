package pl.iza.wspolbiezne.thread;

import pl.iza.wspolbiezne.entity.Professor;
import pl.iza.wspolbiezne.entity.Student;

//interfejs Runnable powstał po to, aby nie trzeba było dziedziczyć z Thread
//bo Java nie ma dziedziczenia wielobazowego, więc utrudniało by to znacząco pracę
//zwykle lepiej jest zaimplementować tylko Runnable i już :)
public class ProfessorConsultationThread implements Runnable {

	public Student student;
	public Professor professor;

	public ProfessorConsultationThread(Professor professor,Student student) {
		this.student = student;
		this.professor = professor;
	}

	public void run() {
		professor.startConsultation();

		try {
			Thread.sleep(student.getTimeNeededToConsult());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Profesor przyj�� studenta: "
				+ this.student.getNameAndSurname());

		professor.endConsultation();
	}


}
