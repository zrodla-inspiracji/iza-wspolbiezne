package pl.iza.wspolbiezne.thread;

import pl.iza.wspolbiezne.entity.Student;

public class StudentConsultationThread extends Thread implements Runnable {

	public Student student;

	public StudentConsultationThread(Student student) {
		this.student = student;
	}

	public void run() {
		System.out.println("Student " + this.student.getNameAndSurname()
				+ " wchodzi na konsultacje");
		try {
			Thread.sleep(student.getTimeNeededToConsult());
			this.student
					.setKnowledge((short) (this.student.getKnowledge() + 1));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
