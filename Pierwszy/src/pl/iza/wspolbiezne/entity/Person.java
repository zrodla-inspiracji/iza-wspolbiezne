package pl.iza.wspolbiezne.entity;

public abstract class Person {

	//Wydaje mi się, że trzymanie imienia i nazwiska w jednym polu jest złym rozwiązaniem
	//Jest coś takiego jak postać normalna bazy danych i jeśli chodzi o struktury danych
	//to powinny być one z nią dość zgodne, niemniej jednak nie ma to znaczenia
	//w tym konkretnym przypadku, więc zostawiam jak było
	protected String nameAndSurname;
	protected Pesel personalId;
	protected short knowledge;
	protected long wealth;
	
	abstract String introduceYourself();

	public short getKnowledge() {
		return knowledge;
	}

	public void setKnowledge(short knowledge) {
		this.knowledge = knowledge;
	}

	public String getNameAndSurname() {
		return nameAndSurname;
	}

	public void setNameAndSurname(String nameAndSurname) {
		this.nameAndSurname = nameAndSurname;
	}
	
	
}
