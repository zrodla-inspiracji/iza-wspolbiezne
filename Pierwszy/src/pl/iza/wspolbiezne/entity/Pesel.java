package pl.iza.wspolbiezne.entity;
import pl.iza.wspolbiezne.util.Check;

/**Klasa Pesel
 * 
 * @author Iza
 * 
 * @version Beta
 * 
 * */
public class Pesel
{
	public int pesel[];
	public boolean valid = true;
	String peselS;
	
	/** Pusty konstruktor*/
	public Pesel()
	{
		
	}
	/** Przypisanie peselu do obiektu pesel
	* @param pes
	* */
	public void getPesel(String peselS){
		pesel = new int[peselS.length()];
		for(int i=0; i<peselS.length();i++)
		{
			pesel[i] = Integer.parseInt(Character.toString(peselS.charAt(i)));
		}
		this.peselS = peselS;
		
	}
	
	public boolean isValid(){
		return valid = Check.isPeselValid(pesel);
	}
	public String getInfo(){
		
		valid = Check.isPeselValid(pesel);
		if(valid)
		{
			return peselS + " " +Check.getBirthDate(pesel) + " " + Check.getSex(pesel);
		}
		else
		{
			return "Pesel niepoprawny";	
		}
	}
	public void printInfo()
	{
		valid = Check.isPeselValid(pesel);
		if(valid)
		{
			System.out.println("Poprawny");
			System.out.println(Check.getBirthDate(pesel));
			System.out.println("P�e�: " + Check.getSex(pesel));
		}
		else
		{
			System.out.println("Niepoprawny");
		}
	}
	
	
}
