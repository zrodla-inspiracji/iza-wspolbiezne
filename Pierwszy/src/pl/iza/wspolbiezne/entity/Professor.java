package pl.iza.wspolbiezne.entity;
import pl.iza.wspolbiezne.thread.ProfessorConsultationThread;


public class Professor extends Person implements Employee {
	
	private static Professor instance;

	public int publicationsNumber;
	public boolean consultations = false;
	
	private Schoolroom schoolroom;
	
	public static Professor getInstance(String name, String id, short knowledge, long wealth, int publications){
		if(instance==null){
			instance = new Professor(name, id, knowledge, wealth, publications);
		}
		return instance;
	}
	
	private Professor(String name, String id, short knowledge, long wealth, int publications){
		
		try
		{
			this.nameAndSurname = name;
			this.personalId = new Pesel();
			personalId.getPesel(id);
			if(!personalId.isValid())
			{
				throw new Exception();
			}
			if(knowledge >= 0)
			{
				this.knowledge = knowledge;
			}
			else
			{
				throw new Exception();
			}	
			if(wealth >= 0)
			{
				this.wealth = wealth;
			}
			else
			{
				throw new Exception();
			}	
			
			if(publications >= 0)
			{
				this.publicationsNumber = publications;
			}
			else
			{
				this.publicationsNumber = 50;
			}
		}
		catch(Exception ex)
		{
			System.out.println("Nieprawid�owo wprowadzone dane");
		}
	}

	public void work(int days) {
		int weeks = days/7;
		this.knowledge = (short) (this.knowledge + weeks);
		int years = days/365;
		this.publicationsNumber = this.publicationsNumber + 5*years;
		int months = days/30;
		this.wealth = this.wealth + 3*months*Professor.MINIMUM_SALARY;
	}

	String introduceYourself() {
		String info;
		info = this.nameAndSurname + " " + this.personalId.getInfo() 
				+ " knowledge: " + this.knowledge + " wealth: " + this.wealth 
				+ " publications: " + this.publicationsNumber;
		return info;
	}
	
	public void examine(Student student){
		double gpa = student.gpa;
		short yos = student.yearOfStudy;
		short know = student.knowledge;
		double new_gpa = 0;
		new_gpa = ((gpa*yos + know/yos)/(yos+1));
		
		if(new_gpa>=2.0 && new_gpa <=5.0)
		{
		 student.gpa = new_gpa;
		 if(student.gpa >= 3.0 && student.yearOfStudy <5) student.yearOfStudy = (short) (student.yearOfStudy+1);
		 
		}		
	}
	
	public void examineAll(StudentList list)
	{
		for(Student s : list.students)
		{
			this.examine(s);
		}
	}
	
	public void receiveStudent(Student student)
	{
		ProfessorConsultationThread profThread = new ProfessorConsultationThread(this,student);
		(new Thread(profThread)).start();
	}
	
	public void endConsultation(){
		consultations=false;
		schoolroom.checkIfThereIsSomeoneElseToConsult();
	}
	
	public void startConsultation(){
		consultations=true;
	}
	
	public Boolean doingConsultations(){
		return consultations;
	}

	public void setSchoolroom(Schoolroom schoolroom) {
		this.schoolroom = schoolroom;
	}

}