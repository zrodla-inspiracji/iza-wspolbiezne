package pl.iza.wspolbiezne.entity;
import pl.iza.wspolbiezne.thread.StudentConsultationThread;


public class Student extends Person{

	private int studentNumber;
	public double gpa;
	public short yearOfStudy;
	
	public Student(String name, String id, short knowledge, int number, double gpa, short yos){
		try
		{
			this.nameAndSurname = name;
			this.personalId = new Pesel();
			personalId.getPesel(id);
			if(!personalId.isValid()){
				throw new Exception();
			}
		
			if(knowledge >=0)
			{
				this.knowledge = knowledge;
			}
			else
			{
				throw new Exception();
			}
		
			this.wealth = 0;
			this.studentNumber = number;
			
			if(gpa>=2.0 && gpa <=5.0 )
			{
				this.gpa = gpa;
			}
			else
			{
				throw new Exception();
			}
		
			if(yos>=1 && yos <=5 )
			{
				this.yearOfStudy = yos;
			}
			else
			{
				throw new Exception();
			}
		}
		catch(Exception ex)
		{
			System.out.println("Nieprawid�owo wprowadzone dane");
		}
		
	}
	
	String introduceYourself() {
		String info;
		info = this.nameAndSurname + " " + this.personalId.getInfo() 
				+ " knowledge: " + this.knowledge + " wealth: " + this.wealth 
				+ " student number: " + this.studentNumber + " gpa: " + this.gpa
				+ " year of study: " + this.yearOfStudy;
		return info;
	}
	
	String introduce() {
		String info;
		info = this.nameAndSurname;
		return info;
	}
	
	public void learn(short a){
		this.knowledge= (short) (this.knowledge + a);
	}
	
	public void party(){
		this.knowledge = (short) (this.knowledge - 1);
	}
	
	public Long getTimeNeededToConsult(){
		return 3000L + getKnowledge() * 100;
	}
	
	public int getStudentNumber()
	{
		return this.studentNumber;
	}
	
	public void consultProfessor()
	{
		StudentConsultationThread studentThread = new StudentConsultationThread(this);
		(new Thread(studentThread)).start();
	}
}
