package pl.iza.wspolbiezne.entity;

public interface Employee {
	public static final int MINIMUM_SALARY = 1000;
	public abstract void work(int days);
}
