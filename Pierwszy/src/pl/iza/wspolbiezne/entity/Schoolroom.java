package pl.iza.wspolbiezne.entity;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Schoolroom {

	private Professor professor;
	
	//Lepiej jest używać interfejsów, niż dokłądnych implementacji
	private Queue<Student> students;

	public Schoolroom(Professor professor) {
		this.professor = professor;
		students = new ConcurrentLinkedQueue<Student>();
		// tutaj jest pewne niedociągnięcie programistyczne
		// Polega ono na zależności cyklicznej
		// zarówno Schoolroom wie o Professor
		// jak i na odwrót Professor wie o Schoolroom
		// powinno się to zrobić inaczej, ale wtedy zrobi się to lekko
		// zagmatfane
		// narazie może tak być :)
		professor.setSchoolroom(this);
	}

	public void tryToBeConsulted(Student student) {
		if (professorConslutingSomeone()) {
			put(student);
		} else {
			professor.receiveStudent(student);
			student.consultProfessor();
		}
	}

	public void checkIfThereIsSomeoneElseToConsult() {
		Student potentialToConsult = get();
		if (potentialToConsult != null) {
			tryToBeConsulted(potentialToConsult);
		}
	}

	private Boolean professorConslutingSomeone() {
		return professor.doingConsultations();
	}

	public void put(Student s) {
		students.add(s);
	}

	public Student get() {
		return students.poll();
	}

	public Student peek() {
		return students.poll();
	}

	public boolean isEmpty() {
		return students.isEmpty();
	}

	public int size() {
		return students.size();
	}

}
