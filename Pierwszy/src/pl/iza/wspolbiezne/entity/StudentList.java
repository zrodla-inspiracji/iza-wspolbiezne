package pl.iza.wspolbiezne.entity;

import java.util.ArrayList;

import pl.iza.wspolbiezne.exception.StudentAlreadyAddedException;
import pl.iza.wspolbiezne.exception.StudentNotFoundException;
import pl.iza.wspolbiezne.exception.WrongNameException;
import pl.iza.wspolbiezne.exception.WrongPeselException;

public class StudentList {

	public ArrayList<Student> students = new ArrayList<Student>();

	public StudentList() {
	}

	public void add(Student s) {
		try {
			char[] ch = s.nameAndSurname.toCharArray();
			int capitals = 0;
			boolean ifSomething = false;
			for (Student st : students) {
				if (st == s) {
					throw new StudentAlreadyAddedException();
				}
			}

			if (!s.personalId.isValid()) {
				throw new WrongPeselException();
			}

			for (char c : ch) {
				int ascii = (int) c;
				if ((ascii >= 65 && ascii <= 90)
						|| (ascii >= 97 && ascii <= 122) || ascii == 32
						|| ascii == 45) {
					if ((ascii >= 65 && ascii <= 90)) {
						capitals++;
					}
					if (ascii == 45) {
						ifSomething = true;
					}
				} else {
					throw new WrongNameException();
				}
			}
			if (capitals <= 2 && ifSomething) {
				throw new WrongNameException();
			}
			students.add(s);
			System.out.println("Student dodany do listy");

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

	public void remove(int st_number) {
		boolean check = true;
		try {
			for (Student s : students) {
				if (s.getStudentNumber() == st_number) {
					students.remove(s);
					System.out.println("Student usuni�ty z listy");
					check = false;
				}
			}

			if (check) {
				throw new StudentNotFoundException();
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public void remove(Pesel pes) {
		boolean check = true;

		try {
			for (Student s : students) {
				if (s.personalId == pes) {
					students.remove(s);
					System.out.println("Student usuni�ty z listy");
					check = false;
				}

			}
			if (check) {
				throw new StudentNotFoundException();
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public int getStudentCount() {
		return students.size();
	}

	public void printStudent(int studentNumber) {
		for (Student s : students) {
			if (s.getStudentNumber() == studentNumber) {
				System.out.println(s.introduceYourself());
			}

		}

	}

	public void listAll(int mode) {

		if (mode == 0) {

			for (Student s : students) {
				System.out.println(s.introduce());
			}

		}

		if (mode == 1) {
			for (Student s : students) {
				System.out.println(s.introduceYourself());
			}
		}
	}

}
