package pl.iza.wspolbiezne.util;

public class Check {
	
	/** Sprawdzanie czy pesel jest poprawny
	 * @param pes
	 * @return boolean
	 * */
	public static boolean isPeselValid(int pes[])
	{
		
		if(pes.length != 11) return false;
		if(pes[2]<0 ||pes[2]>3) return false;
		if(pes[4]<0 ||pes[4]>3) return false;
		return true;
	}
	
	/** Zwraca dat� urodzenia na podstawie podanego argumentu
	 * @param pes
	 * @return String
	 * */
	public static String getBirthDate(int pes[])
	{
		String date;
		if(pes[2] == 0 || pes[2] == 1)
		{
			date = "Date: " + pes[4] + pes[5] +"-" + pes[2] + pes[3] +"-19" + pes[0] + pes[1];
			return date;
		}
		if(pes[2] == 2 || pes[2] == 3)
		{
			int month = pes[2] - 2;
			date = "Date: " + pes[4] + pes[5] +"-" + month + pes[3] +"-20" + pes[0] + pes[1];
			return date;
		}
		
		return null;
	}
	/** Zwraca p�e� na podstawie podanego argumentu
     * @param pes
	 * @return String
	 * */
	public static String getSex(int pes[])
	{
		if(pes[9]%2 == 0) return "Kobieta";
		if(pes[9]%2 == 1) return "M�czyzna";
		
		return null;
		
	}
}
