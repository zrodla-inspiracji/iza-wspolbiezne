package pl.iza.wspolbiezne;
import pl.iza.wspolbiezne.entity.Schoolroom;
import pl.iza.wspolbiezne.entity.Professor;
import pl.iza.wspolbiezne.entity.Student;

public class Main {

	//Droga Izo, słowem wstępu: to, że mam jakieś tam uwagi to nie znaczy, że to jest źle!
	//Znaczy to, że moim zdaniem można to było (z pewnych powodów) zrobić inaczej
	//Mam też nadzieję, że nie będziesz smutna jak coś tam poprawię
	//Chciałbym, żebyś przyjęła, że to nie implikuje tego, że "totalnie zjebałaś"
	//Najważniejsze jest to, żebyś się uczyła i poznawała nowe rzeczy
	//Ja z chęcią Ci pomogę
	
	public static void main(String[] args) {

		short yos = 2;
		Student student1 = new Student("Adam Kowalski", "92160614193",(short) 10,175456,3.53,yos);
		Student student2 = new Student("Anna Kowalski", "92160614493",(short) 9,175789,2.03,yos);
		Student student3 = new Student("Daniel Kowalski", "92160614445",(short) 11,175478,2.03,yos);
		Student student4 = new Student("Maja Kotlicka", "94160614293",(short) 12,175222,3.03,yos);
		Student student5 = new Student("Maja Kotlicka-Osa", "94160674293",(short) 5,175223,4.03,yos);
		Student student6 = new Student("Maja Kot", "94160614243",(short) 11,175242,2.03,yos);
		
		Professor prof = Professor.getInstance("Michał Sam", "52121115679",(short) 100,3000,120);
		
		Schoolroom schoolroom = new Schoolroom(prof);
		schoolroom.tryToBeConsulted(student1);
		schoolroom.tryToBeConsulted(student2);
		
		schoolroom.tryToBeConsulted(student3);
		schoolroom.tryToBeConsulted(student6);
		schoolroom.tryToBeConsulted(student5);
		schoolroom.tryToBeConsulted(student6);
		schoolroom.tryToBeConsulted(student4);
		schoolroom.tryToBeConsulted(student5);
		schoolroom.tryToBeConsulted(student6);
	
		System.out.println("Student1 wiedza: " + student1.getKnowledge());
		System.out.println("Student2 wiedza: " + student2.getKnowledge());
		System.out.println("Student3 wiedza: " + student3.getKnowledge());
		System.out.println("Student4 wiedza: " + student4.getKnowledge());
		System.out.println("Student5 wiedza: " + student5.getKnowledge());
		System.out.println("Student6 wiedza: " + student6.getKnowledge());
	}
}
